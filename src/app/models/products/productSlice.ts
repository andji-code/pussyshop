import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../../store";
import { Product } from "./types";
import { getProducts } from "../../../api/api";

// Define the initial state using that type
const initialState = {
  products: null as { [key: Product["_id"]]: Product } | null,
  isLoading: false,
};

export const productSlice = createSlice({
  name: "product",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // increment: (state) => {
    //   state.value += 1;
    // },
    // decrement: (state) => {
    //   state.value -= 1;
    // },
    // // Use the PayloadAction type to declare the contents of `action.payload`
    // incrementByAmount: (state, action: PayloadAction<number>) => {
    //   state.value += action.payload;
    // },
  },
  extraReducers: (builder) => {
    builder.addCase(getProductsThunk.pending, (s, a) => {
      s.isLoading = true;
    });
    builder.addCase(getProductsThunk.fulfilled, (s, a) => {
      const products: { [key: Product["_id"]]: Product } = {};
      a.payload.forEach((product) => (products[product._id] = product));

      s.products = products;
      s.isLoading = false;
    });
    builder.addCase(getProductsThunk.rejected, (s, a) => {
      s.isLoading = false;
    });
  },
});

export const getProductsThunk = createAsyncThunk("products/get", getProducts);

// Other code such as selectors can use the imported `RootState` type
export const selectProducts = (state: RootState) => state.product.products!;

export const productReducer = productSlice.reducer;
