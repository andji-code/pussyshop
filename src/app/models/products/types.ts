import { MongoItem } from "../../../helpers/types";

export type Product = {
  name: string;
  price: number;
  img: string;
  isInCart: boolean;
} & MongoItem;

export type ProductsMap = { [key: Product["_id"]]: Product };
