import { Product } from "./../products/types";
import {
  addCart,
  addCount,
  addOrder,
  delCart,
  getCart,
} from "./../../../api/api";
import {
  createAction,
  createAsyncThunk,
  createReducer,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import type { RootState } from "../../store";
import { CartType, Order } from "./types";
import { getProducts } from "../../../api/api";

// Define the initial state using that type
type InitState = {
  isFetching: boolean;
  isOrderFetching: boolean;
} & CartType;

const initialState: InitState = {
  items: null as { [key: Product["_id"]]: number } | null,
  totalPrice: 0,
  isFetching: false,
  isOrderFetching: false,
};

export const makeOrder = createAsyncThunk("cart/order", addOrder);

export const getCartThunk = createAsyncThunk("cart/get", getCart);
export const addCartThunk = createAsyncThunk("cart/add", addCart);
export const addCartCountThunk = createAsyncThunk("cart/add/count", addCount);
export const delCartThunk = createAsyncThunk("cart/del", delCart);

export const cartSlice = createSlice({
  name: "cart",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {},
  extraReducers: (b) => {
    b.addCase(getCartThunk.pending, (s, a) => {
      s.isFetching = true;
    });
    b.addCase(getCartThunk.fulfilled, (s, a) => {
      console.log("cartExist");
      s.items = a.payload.items;
      s.totalPrice = a.payload.totalPrice;
      s.isFetching = false;
    });
    b.addCase(getCartThunk.rejected, (s, a) => {
      s.isFetching = false;
    });

    b.addCase(makeOrder.pending, (s, a) => {
      s.isOrderFetching = true;
    });
    b.addCase(makeOrder.fulfilled, (s, a) => {
      s.isOrderFetching = false;
    });
    b.addCase(makeOrder.rejected, (s, a) => {
      s.isOrderFetching = false;
    });

    b.addCase(addCartThunk.pending, (s, a) => {
      s.isFetching = true;
    });
    b.addCase(addCartThunk.fulfilled, (s, a) => {
      s.items = a.payload.items;
      s.totalPrice = a.payload.totalPrice;
      s.isFetching = false;
    });
    b.addCase(addCartThunk.rejected, (s, a) => {
      s.isFetching = false;
    });

    b.addCase(delCartThunk.pending, (s, a) => {
      s.isFetching = true;
    });
    b.addCase(delCartThunk.fulfilled, (s, a) => {
      s.items = a.payload.items;
      s.totalPrice = a.payload.totalPrice;
      s.isFetching = false;
    });
    b.addCase(delCartThunk.rejected, (s, a) => {
      s.isFetching = false;
    });

    b.addCase(addCartCountThunk.pending, (s, a) => {
      s.isFetching = true;
    });
    b.addCase(addCartCountThunk.fulfilled, (s, a) => {
      s.items = a.payload.items;
      s.totalPrice = a.payload.totalPrice;
      s.isFetching = false;
    });
    b.addCase(addCartCountThunk.rejected, (s, a) => {
      s.isFetching = false;
    });
  },
});

// Other code such as selectors can use the imported `RootState` type
export const selectCart = (state: RootState) => state.cart;
export const selectCartPrice = (state: RootState) => state.cart.totalPrice;
// export const selectIsProductInCart =
//   (productId: Product["_id"]) => (state: RootState) =>
//     !!state.cart.items[productId];

export const cartReducer = cartSlice.reducer;
