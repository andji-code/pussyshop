import { Product } from "./../products/types";
import { CartFormData } from "./../../../pages/Cart/CartForm";
import { MongoItem } from "../../../helpers/types";

export type CartItemType = {
  count: number;
  product: Product;
};

export type CartElementType = {
  count: number;
  productId: Product["_id"];
};

export type CartItemsMap = { [key: Product["_id"]]: number };

export type CartType = {
  items: CartItemsMap | null;
  totalPrice: number;
};

export type OrderItem = {
  count: number;
  productId: Product["_id"];
};

export type Order = {
  order: OrderItem[];
} & CartFormData;
