import "./styles/reset.css";
import "./styles/vars.css";
import "./styles/main.css";
import { Header } from "./ui/Header/Header";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { useEffect } from "react";
import {
  selectProducts,
  getProductsThunk,
} from "./app/models/products/productSlice";
import { getProducts } from "./api/api";
import { ReactComponent as Loader } from "./assets/loader.svg";
import { ProductCard } from "./components/ProductCard/ProductCard";
import { Route, Routes } from "react-router";

import s from "./styles/app.module.css";
import { CartPage } from "./pages/Cart/CartPage";
import { HomePage } from "./pages/Home/HomePage";
import { getCartThunk, selectCart } from "./app/models/cart/cartSlice";

function App() {
  const products = useAppSelector(selectProducts);
  const cart = useAppSelector(selectCart);
  const d = useAppDispatch();

  useEffect(() => {
    d(getProductsThunk());
    d(getCartThunk());
  }, [d]);

  if (!products || !cart.items) return <Loader />;

  return (
    <div className={s.app}>
      <Header />
      <main className={s.main}>
        <Routes>
          <Route path="/" element={<HomePage products={products} />} />
          <Route path="/cart" element={<CartPage />} />
        </Routes>
      </main>
    </div>
  );
}

export default App;
