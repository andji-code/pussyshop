import { FC } from "react";
import s from "./text.module.css";
import cn from "classnames";

type Props = {
  size?: "1" | "2" | "3" | "4" | "5";
  isBold?: boolean;
  theme?: "light" | "dark";
  isLink?: boolean;
  classNames?: string;
};

export const Text: FC<Props> = ({
  size = "3",
  isBold = false,
  theme = "light",
  isLink = false,
  classNames = "",
  children,
}) => {
  return (
    <p
      className={cn([
        classNames,
        s.font,
        s[`size${size}`],
        s[theme],
        { [s.bold]: isBold },
        { [s.link]: isLink },
      ])}
    >
      {children}
    </p>
  );
};
