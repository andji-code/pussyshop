import React from "react";
import { ReactComponent as LogoIcon } from "../../assets/logo.svg";
import { ReactComponent as CartIcon } from "../../assets/cart.svg";
import s from "./header.module.css";
import { Text } from "../Text/Text";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { selectCartPrice } from "../../app/models/cart/cartSlice";

export function Header() {
  const d = useAppDispatch();
  const price = useAppSelector(selectCartPrice);

  return (
    <header className={s.header}>
      <LogoIcon className={s.logo} />
      <Link to="/" className={s.link}>
        <Text isLink>Pussy Shop</Text>
      </Link>
      <Link to="/cart">
        <div className={s.cartPanel}>
          <Text>{price === 0 ? "" : `$${price}`}</Text>
          <CartIcon className={s.cart} />
          <Text>|</Text>
          <Text>My Cart</Text>
        </div>
      </Link>
    </header>
  );
}
