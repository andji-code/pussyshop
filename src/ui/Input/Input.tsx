import { FC } from "react";
import s from "./input.module.css";
import cn from "classnames";
import { UseFormRegister, UseFormRegisterReturn } from "react-hook-form";

type Props = {
  placeholder?: string;
  registerReturn: UseFormRegisterReturn;
};

export const Input = ({ placeholder = "", registerReturn }: Props) => {
  return (
    <input {...registerReturn} className={s.input} placeholder={placeholder} />
  );
};
