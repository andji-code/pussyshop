import { FC } from "react";
import s from "./button.module.css";
import cn from "classnames";
import { Text } from "../Text/Text";
import { ReactComponent as Loader } from "../../assets/loader.svg";

type Props = {
  onClick?: () => void;
  isDisabled?: boolean;
  theme?: "main" | "alt" | "success";
  isWide?: boolean;
  isLoading?: boolean;
  classNames?: string;
};

export const Button: FC<Props> = ({
  isDisabled = false,
  isLoading = false,
  isWide = false,
  classNames = "",
  theme = "main",
  onClick,
  children,
}) => {
  return (
    <button
      onClick={onClick}
      disabled={isDisabled}
      className={cn([
        classNames,
        s.btn,
        { [s.disabled]: isDisabled },
        { [s.wide]: isWide },
        s[theme],
      ])}
    >
      {isLoading ? (
        <Loader height={100} className={s.loader} />
      ) : (
        <Text>{children}</Text>
      )}
    </button>
  );
};
