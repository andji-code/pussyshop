import { useAppDispatch } from "../../app/hooks";
import {
  addCartCountThunk,
  addCartThunk,
  delCartThunk,
} from "../../app/models/cart/cartSlice";
import { CartItemType } from "../../app/models/cart/types";
import { Button } from "../../ui/Button/Button";
import { Text } from "../../ui/Text/Text";

import s from "./cartCard.module.css";

type Props = {
  cartItem: CartItemType;
};

const CartCard = ({ cartItem }: Props) => {
  const { count, product } = cartItem;
  const { img, name, price } = product;

  const d = useAppDispatch();

  const addToCart = () => {
    d(addCartCountThunk(product._id));
  };

  const delFromCart = () => {
    d(delCartThunk(product._id));
  };

  return (
    <div className={s.card}>
      <img src={img} alt={name} className={s.img} />
      <div className={s.info}>
        <div className={s.text}>
          <Text size="5" theme="dark" classNames={s.name}>
            {name}
          </Text>
          <div className={s.note}>
            <Text size="2" theme="dark">
              Fluffy, happy, sweet and cute,
            </Text>
            <Text size="2" theme="dark">
              take a kitty faster dude!
            </Text>
            <Text classNames={s.price} isBold size="2" theme="dark">
              {`$${price}`}
            </Text>
          </div>
        </div>
        <div className={s.countControls}>
          <Button classNames={s.btn} onClick={delFromCart}>
            -
          </Button>
          <Text theme="dark" size="5">
            {count}
          </Text>
          <Button classNames={s.btn} onClick={addToCart}>
            +
          </Button>
        </div>
      </div>
    </div>
  );
};

export default CartCard;
