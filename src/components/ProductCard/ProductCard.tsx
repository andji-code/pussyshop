import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { Product } from "../../app/models/products/types";
import { Button } from "../../ui/Button/Button";
import { Text } from "../../ui/Text/Text";
import s from "./productCard.module.css";
import { addCartThunk, selectCart } from "../../app/models/cart/cartSlice";
import { useState } from "react";

export function ProductCard(product: Product) {
  const { img, name, price, _id } = product;
  const cartItems = useAppSelector(selectCart).items;
  const d = useAppDispatch();

  const addToCart = () => {
    d(addCartThunk(_id));
  };

  if (!cartItems) return null;

  return (
    <div className={s.card}>
      <img src={img} className={s.img} alt={name} />
      <div className={s.info}>
        <Text size="5" theme="dark" classNames={s.name}>
          {name}
        </Text>
        <Text size="2" theme="dark">
          Fluffy, happy, sweet and cute,
        </Text>
        <Text size="2" theme="dark">
          take a kitty faster dude!
        </Text>
        {cartItems[_id] ? (
          <Button onClick={addToCart} classNames={s.btn} isWide isDisabled>
            IN CART!
          </Button>
        ) : (
          <Button
            onClick={addToCart}
            classNames={s.btn}
            isWide
          >{`$${price}`}</Button>
        )}
      </div>
    </div>
  );
}
