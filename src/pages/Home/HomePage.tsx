import { FC } from "react";
import { ProductsMap } from "../../app/models/products/types";
import { ProductCard } from "../../components/ProductCard/ProductCard";
import s from "./homePage.module.css";

type Props = {
  products: ProductsMap;
};

export const HomePage: FC<Props> = ({ products }) => {
  const productList = Object.values(products);
  return (
    <div className={s.list}>
      {productList.map((p) => (
        <ProductCard key={p._id} {...p} />
      ))}
    </div>
  );
};
