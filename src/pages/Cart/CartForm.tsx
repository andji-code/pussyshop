import classNames from "classnames";
import React from "react";
import { useForm } from "react-hook-form";
import { selectProducts } from "../../app/models/products/productSlice";
import { Button } from "../../ui/Button/Button";
import { Input } from "../../ui/Input/Input";
import { Text } from "../../ui/Text/Text";
import s from "./cart.module.css";

export type CartFormData = {
  name: string;
  surname: string;
  address: string;
  phone: string;
};

type Props = {
  isBtnDisabled?: boolean;
  isFetching?: boolean;
  isSuccess?: boolean;
  onSubmit: (data: CartFormData) => void;
};

export const CartForm = ({
  isBtnDisabled = false,
  onSubmit,
  isSuccess = false,
  isFetching = false,
}: Props) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<CartFormData>();

  const isError = !!(
    errors.name ||
    errors.surname ||
    errors.address ||
    errors.phone
  );

  return (
    <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
      <Input
        registerReturn={register("name", { required: true })}
        placeholder="NAME"
      />
      <Input
        registerReturn={register("surname", { required: true })}
        placeholder="SURNAME"
      />
      <Input
        registerReturn={register("address", { required: true })}
        placeholder="ADDRESS"
      />
      <Input
        registerReturn={register("phone", { required: true })}
        placeholder="PHONE"
      />
      {isError ? (
        <Text classNames={s.errorTxt} theme="dark" size="3">
          All fields are required!
        </Text>
      ) : (
        <Text classNames={s.errorTxt} size="3">
          {" "}
        </Text>
      )}
      <Button
        isLoading={isFetching}
        theme={isSuccess ? "success" : "main"}
        isDisabled={isBtnDisabled || isError || isFetching || isSuccess}
        isWide
        classNames={s.submit}
      >
        {isSuccess ? "ORDERED SUCCESS" : "ORDER"}
      </Button>
    </form>
  );
};

export default CartForm;
