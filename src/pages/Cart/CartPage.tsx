import { FC, useEffect, useState } from "react";

import s from "./cart.module.css";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { makeOrder, selectCart } from "../../app/models/cart/cartSlice";
import CartCard from "../../components/CartCard/CartCard";
import CartForm, { CartFormData } from "./CartForm";
import { Text } from "../../ui/Text/Text";
import cn from "classnames";
import { Order, OrderItem } from "../../app/models/cart/types";
import { selectProducts } from "../../app/models/products/productSlice";

export const CartPage: FC = () => {
  const cart = useAppSelector(selectCart);
  const products = useAppSelector(selectProducts);

  const d = useAppDispatch();

  const itemList = Object.entries(cart.items!).map(([productId, count]) => ({
    count,
    product: products[productId],
  }));

  const isCartEmpty = itemList.length === 0;
  const isLess3 = itemList.length < 3;

  const [isSuccess, setIsSuccess] = useState<boolean | null>(null);

  useEffect(() => {
    if (cart.isOrderFetching === true) setIsSuccess(false);
    if (cart.isOrderFetching === false && isSuccess === false)
      setIsSuccess(true);
  }, [cart.isOrderFetching, isSuccess]);

  const onSubmit = (data: CartFormData) => {
    const order: OrderItem[] = itemList.map((i) => ({
      count: i.count,
      productId: i.product._id,
    }));
    const orderData: Order = { ...data, order };
    d(makeOrder(orderData));
  };

  return (
    <div className={s.page}>
      <div
        className={cn([
          s.items,
          { [s.emptyText]: isCartEmpty },
          { [s.less3]: isLess3 },
        ])}
      >
        {isCartEmpty && (
          <Text theme="dark" isBold>
            (YOUR CART IS EMPTY)
          </Text>
        )}
        {itemList.map((item) => (
          <CartCard key={item.product._id} cartItem={item} />
        ))}
      </div>
      <CartForm
        isFetching={cart.isOrderFetching}
        isSuccess={!!isSuccess}
        onSubmit={onSubmit}
        isBtnDisabled={isCartEmpty}
      />
      <Text theme="dark" isBold classNames={s.total}>
        TOTAL: {`$${cart.totalPrice}`}
      </Text>
    </div>
  );
};
