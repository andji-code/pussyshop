import { CartItemsMap, CartType } from "../app/models/cart/types";
import { Product } from "./../app/models/products/types";

export type CartTypeRes = {
  items: CartItemsMap;
  totalPrice: number;
};
