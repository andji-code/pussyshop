import { CartElementType, Order } from "./../app/models/cart/types";
import axios from "axios";
import { Product } from "../app/models/products/types";
import { CartTypeRes } from "./types";

export const api = axios.create({
  // baseURL: "http://localhost:5000",
  baseURL: "https://lampa-pussy-shop-server.herokuapp.com",
});

export type OrderData = Order;

export const getProducts = async () => {
  const { data } = await api.get<Product[]>("/products");
  console.log({ data });
  return data;
};

export const getCart = async () => {
  const { data } = await api.get<CartTypeRes>("/cart");
  console.log(data);
  return data;
};

export const addOrder = async (order: Order) => {
  const { data, status } = await api.post("/orders", order);
  console.log("orderRes");
  console.log({ data, status });
  return { isOk: true };
};

export const addCart = async (productId: Product["_id"]) => {
  const { data } = await api.post<CartTypeRes>(`/cart/${productId}`);
  return data;
};

export const delCart = async (productId: Product["_id"]) => {
  const { data, status } = await api.delete<CartTypeRes>(`/cart/${productId}`);
  return data;
};

export const addCount = async (productId: Product["_id"]) => {
  const { data, status } = await api.put<CartTypeRes>(`/cart/${productId}`);
  return data;
};
